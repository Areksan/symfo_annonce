N'oubliez pas de faire :
=============================
```
composer install
symfony console doctrine:database:create
symfony console make:migration
symfony console doctrine:migrations:migrate
```
Vous pouvez même le copié coller 

BDD
================================
Si localement vous voulez changer le nom de la BDD aller dans env.
`DATABASE_URL=mysql://root:@127.0.0.1:3306/annonce2?serverVersion=5.7`  

Pour  `DATABASE_URL=mysql://root:@127.0.0.1:3306/**NomDeLaBdd**?serverVersion=5.7 `

Dans le cadre ou le projet est déjà installer sur votre machine :
===============================
Il est possible que vous ayez après une récupération de fichier besoin de mettre à jour :  

` symfony console make:migration `  

`symfony console doctrine:migrations:migrate`

Wiki Information :
=
##### [Wiki](https://gitlab.com/Areksan/symfo_annonce/-/wikis/home)
Wiki Home Page
##### [Controller Wiki Index](https://gitlab.com/Areksan/symfo_annonce/-/wikis/Controller/Index-Controller)
Contient les informations sur l'ensemble des Controllers.
