<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Img
 *
 * @ORM\Table(name="img",indexes={@ORM\Index(name="I_FK_IMG_ANNONCE", columns={"ID_ANNONCE"})} )
 * @ORM\Entity(repositoryClass="App\Repository\ImgRepository")
 */
class Img
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID_IMG", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idImg;

    /**
     * @var string
     *
     * @ORM\Column(name="PATH", type="string", length=256, nullable=false)
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Annonce", inversedBy="imgs")
     * @ORM\JoinColumn(name="ID_ANNONCE", referencedColumnName="ID_ANNONCE",nullable=false, onDelete="CASCADE")
     */
    private $ID_ANNONCE;

    public function getIdImg(): ?int
    {
        return $this->idImg;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function getIDANNONCE(): ?Annonce
    {
        return $this->ID_ANNONCE;
    }

    public function setIDANNONCE(?Annonce $ID_ANNONCE): self
    {
        $this->ID_ANNONCE = $ID_ANNONCE;
        return $this;
    }
}
