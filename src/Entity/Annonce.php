<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Validator as AcmeAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Annonce
 *
 * @ORM\Table(name="annonce", indexes={@ORM\Index(name="I_FK_ANNONCE_RUBRIQUE", columns={"ID_RUBRIQUE"})})
 * @ORM\Entity(repositoryClass="App\Repository\AnnonceRepository")
 */
class Annonce
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID_ANNONCE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idAnnonce;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="EN_TETE_ANNONCE", type="string", length=128, nullable=false)
     */
    private $enTeteAnnonce;

    /**
     * @var string
     * @Assert\NotBlank
     * @ORM\Column(name="CORPS_ANNONCE", type="text", length=65535, nullable=false)
     */
    private $corpsAnnonce;

    /**
     * @var \DateTime
     * @ORM\Column(name="DATE_ONLINE_ANNONCE", type="date", nullable=false)
     */
    private $dateOnlineAnnonce;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DATE_LIM_ANNONCE", type="date", nullable=false)
     */
    private $dateLimAnnonce;

    /**
     * @var \Rubrique
     *
     * @ORM\ManyToOne(targetEntity="Rubrique")
     * @ORM\JoinColumn(name="ID_RUBRIQUE", referencedColumnName="ID_RUBRIQUE", nullable=false, onDelete="CASCADE")
     */
    private $idRubrique;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="annonces")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     */

    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Img", mappedBy="ID_ANNONCE",cascade={"persist"})
     */
    private $imgs;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    private $compteur;

    public function __construct()
    {
        $this->imgs = new ArrayCollection();
    }

    public function getIdAnnonce(): ?string
    {
        return $this->idAnnonce;
    }

    public function getEnTeteAnnonce(): ?string
    {
        return $this->enTeteAnnonce;
    }

    public function setEnTeteAnnonce(string $enTeteAnnonce): self
    {
        $this->enTeteAnnonce = $enTeteAnnonce;
        return $this;
    }

    public function getCorpsAnnonce(): ?string
    {
        return $this->corpsAnnonce;
    }

    public function setCorpsAnnonce(string $corpsAnnonce): self
    {
        $this->corpsAnnonce = $corpsAnnonce;
        return $this;
    }

    public function getDateOnlineAnnonce(): ?\DateTimeInterface
    {
        return $this->dateOnlineAnnonce;
    }

    public function setDateOnlineAnnonce(\DateTimeInterface $dateOnlineAnnonce): self
    {
        $this->dateOnlineAnnonce = $dateOnlineAnnonce;
        return $this;
    }

    public function getDateLimAnnonce(): ?\DateTimeInterface
    {
        return $this->dateLimAnnonce;
    }

    public function setDateLimAnnonce(\DateTimeInterface $dateLimAnnonce): self
    {
        $this->dateLimAnnonce = $dateLimAnnonce;
        return $this;
    }

    public function getIdRubrique(): ?Rubrique
    {
        return $this->idRubrique;
    }

    public function setIdRubrique(?Rubrique $idRubrique): self
    {
        $this->idRubrique = $idRubrique;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;
        return $this;
    }

    /**
     * @return Collection|Img[]
     */
    public function getImgs(): Collection
    {
        return $this->imgs;
    }

    public function addImg(Img $img): self
    {
        if (!$this->imgs->contains($img)) {
            $this->imgs[] = $img;
            $img->setIDANNONCE($this);
        }
        return $this;
    }

    public function removeImg(Img $img): self
    {
        if ($this->imgs->contains($img)) {
            $this->imgs->removeElement($img);
            // set the owning side to null (unless already changed)
            if ($img->getIDANNONCE() === $this) {
                $img->setIDANNONCE(null);
            }
        }
        return $this;
    }

    public function getCompteur(): ?string
    {
        return $this->compteur;
    }

    public function setCompteur(?string $compteur): self
    {
        $this->compteur = $compteur;
        return $this;
    }
}
