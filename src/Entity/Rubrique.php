<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rubrique
 *
 * @ORM\Table(name="rubrique", uniqueConstraints={@ORM\UniqueConstraint(name="LIBELLE_RUBRIQUE", columns={"LIBELLE_RUBRIQUE"})})
 * @ORM\Entity(repositoryClass="App\Repository\RubriqueRepository")
 */
class Rubrique
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID_RUBRIQUE", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idRubrique;

    /**
     * @var string
     *
     * @ORM\Column(name="LIBELLE_RUBRIQUE", type="string", length=64, nullable=false)
     */
    private $libelleRubrique;

    /**
     * @ORM\ManyToOne(targetEntity="Rubrique")
     * @ORM\JoinColumn(name="ID_PARENT", referencedColumnName="ID_RUBRIQUE",nullable=true)
     */
    private $idParent;

    public function getIdRubrique(): ?int
    {
        return $this->idRubrique;
    }

    public function getLibelleRubrique(): ?string
    {
        return $this->libelleRubrique;
    }

    public function setLibelleRubrique(string $libelleRubrique): self
    {
        $this->libelleRubrique = $libelleRubrique;
        return $this;
    }

    public function getIdParent(): ?self
    {
        return $this->idParent;
    }

    public function setIdParent(?self $IdParent): self
    {
        $this->idParent = $IdParent;

        return $this;
    }
}
