<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Entity\Rubrique;
use App\Entity\User;
use App\Entity\Img;
use App\Form\CreateAnnonceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class MainController extends AbstractController
{
    /**
     * @Route("", name="main")
     */
    public function index(Request $request)
    {
        $imgLi = [];
        $rubLi = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findAll();
        $imgLi=null;   
        $annonceLi = $this->getDoctrine()->getRepository(Annonce::class)
            ->findBy([], ["dateOnlineAnnonce" => 'DESC']);
            
            foreach($annonceLi as $ann){
                $imgLi[] = $this->getDoctrine()->getRepository(Img::class)->findOneBy(['ID_ANNONCE' => $ann->getIdAnnonce()]);
            }
        $form = $this->rubriqueTab();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            return $this->redirectToRoute('annonceByRubriqueId', ['id' => $request->get('form')['categories']]);
        }
        return $this->render('main/index.html.twig', [
            'annonceLi' => $annonceLi,
            'rubLi' => $rubLi,
            'imgLi' => $imgLi,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newAnnonce", name="newAnnonce")
     * @param Request $request
     */
    public function AddAnnonce(Request $request)
    {
        $annonce = new Annonce();
        $form = $this->createForm(CreateAnnonceType::class, $annonce, ['tab' => $this->tabRubrique()]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get('image')) {
                $images = $request->files->get('image');// tableau d'images
                for ($i = 0; $i < count($images); $i++) {
                    //enregistre les fichiers images dans public/uploads
                    $uploadedFile = $images[$i];
                    $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
                    $uploadedFile->move($destination, $uploadedFile->getClientOriginalName());
                    //enregistre les fichiers dans la bdd
                    $img = new Img();
                    $img->setPath($uploadedFile->getClientOriginalName());
                    $annonce->addImg($img);
                }
            }
            $annonce->setUser($this->getUser());
            $annonce->setCompteur(0);
            $addAnnonce = $this->getDoctrine()->getManager();
            $addAnnonce->persist($annonce);
            $addAnnonce->flush();
            return $this->redirectToRoute('main');
        }
        return $this->render('main/addAnnonce.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/annonce/{id}",name="annonceById")
     */
    public function annonceById( SessionInterface $session,Request $request, $id)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)->findOneBy(['idAnnonce' => $id]);
        $imgLi[] = $this->getDoctrine()->getRepository(Img::class)
                    ->findOneBy(['ID_ANNONCE' => $annonce->getIdAnnonce()]);
        if (!$session->get('visite' . $id)) {
            $session->set('visite' . $id, true);
            $compteur = $annonce->getCompteur();
            $annonce->setCompteur($compteur + 1);
            $majcompteur = $this->getDoctrine()->getManager();
            $majcompteur->persist($annonce);
            $majcompteur->flush();
        }


        $form = $this->rubriqueTab();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('annonceByRubriqueId', ['id' => $request->get('form')['categories']]);
        }

        return $this->render('main/affAnnonceUnique.html.twig',
            [
                'annonce' => $annonce,
                'imgLi' => $imgLi,
                'form' => $form->createView(),
            ]);
    }

    /**
     * @Route("/rubrique/{id}", name="annonceByRubriqueId")
     */
    public function annonceByRubriqueId(Request $request, $id)
    {
        $annonceLi=$this->annonceLi($id);
        if ($annonceLi) {
            foreach($annonceLi as $ann){
                $imgLi[] = $this->getDoctrine()->getRepository(Img::class)
                    ->findOneBy(['ID_ANNONCE' => $ann->getIdAnnonce()]);
            }

            $form = $this->rubriqueTab();
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                return $this->redirectToRoute('annonceByRubriqueId', ['id' => $request->get('form')['categories']]);
            }
            return $this->render('main/index.html.twig',
                ['annonceLi' => $annonceLi,
                'form' => $form->createView()]
            );
        } else {
            return $this->redirectToRoute('main');
        }
    }

    /**
     * @Route("/user/{id}", name="annonceByUser")
     */
    public function annonceByUser($id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)
            ->findBy(['username' => $id]);
        if (count($user) !== 0) {
            $annonceLi = $this->getDoctrine()->getRepository(Annonce::class)
                ->findBy(['User' => $user], ['dateOnlineAnnonce' => 'DESC']);
            return $this->render('main/index.html.twig',
                [
                    'annonceLi' => $annonceLi,
                ]);
        } else {
            return $this->redirectToRoute('main');
        }
    }

    private function tabRubrique($idParent = null, $sub_mark = "", $tabrub = [])
    {
        $data = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findBy(['idParent' => $idParent], ['libelleRubrique' => 'ASC']);
        foreach ($data as $item) {
            $tabrub[$sub_mark . $item->getLibelleRubrique()] = $item;
            $tabrub = self::tabRubrique($item->getIdRubrique(), $sub_mark . '--', $tabrub);
        }
        return $tabrub;
    }

    private function rubriqueTab()
    {
        $rubLI = $this->tabRubrique() ;
        $rubLiChoices = [];
        foreach ( $rubLI as $key => $item){
            $rubLiChoices[$key] = $item->getIdRubrique();
        }
        $form = $this->createFormBuilder()
            ->add('categories', ChoiceType::class, [
                'choices' => $rubLiChoices,
            ])
            ->getForm();
        return $form;
    }

    private function annonceLi($rubId){
        $rubriqueLi=explode("/",$rubId.$this->rubriqueLi($rubId));
        $annonceLi=null;//tableau des annonces rubrique+subrubriques
        for($i=0;$i<count($rubriqueLi);$i++){     
            // tableau d'annonces d'une rubrique
            $annonces = $this->getDoctrine()->getRepository(Annonce::class)
                ->findBy(['idRubrique' => $rubriqueLi[$i]], ['dateOnlineAnnonce' => 'DESC']); 
            foreach ($annonces as $ann){
                $annonceLi[]=$ann;
            } 
        }
        return $annonceLi;
    }
    private function rubriqueLi($idParent=null){
        $data = $this->getDoctrine()->getRepository(Rubrique::class)
                ->findBy(['idParent'=>$idParent],['libelleRubrique'=>'ASC']);
        $select = "";
        foreach($data as $j){  
            $select.= "/".$j->getIdRubrique();
            $select.=self::rubriqueLi($j->getIdRubrique());
        }
        return $select; 
    }

//    private $tab=array();
//    public function getTab(){
//        return $this->tab;
//    }
//    public function getParents($idRubrique){
//        $data = $this->getDoctrine()->getRepository(Rubrique::class)
//            ->findOneBy(['idRubrique'=>$idRubrique]);
//        $this->tab[]=$data->getLibelleRubrique();
//         if ($data->getIdParent()!=NULL){
//            self::getParents($data->getIdParent());
//        }
//        $fil=implode(">",array_reverse($this->tab));
//        return $fil;
//    }
}
