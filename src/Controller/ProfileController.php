<?php

namespace App\Controller;

use App\Entity\Rubrique;
use App\Entity\User;
use App\Entity\Annonce;
use App\Entity\Img;
use App\Form\CreateAnnonceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/profile", name="profile")
 */
class ProfileController extends AbstractController
{
    /**
     * @Route("",name="")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $imgLi=[];
        $annonceLi = $this->getDoctrine()->getRepository(Annonce::class)
        ->findBy(['User' => $this->getUser()]);
        foreach($annonceLi as $ann){
            $imgLi[] = $this->getDoctrine()->getRepository(Img::class)
                ->findOneBy(['ID_ANNONCE' => $ann->getIdAnnonce()]);
        }
        $user = $this->getUser();
        return $this->render('profile/index.html.twig', [
            'imgLi' => $imgLi,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/delete", name="_delete")
     */
    public function delete(Request $request)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)
            ->findOneBy(['idAnnonce' => $request->get('idAnnonce'), 'User' => $this->getUser()]);
        $this->removeImages($annonce);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($annonce);
        $manager->flush();
        return $this->redirectToRoute('profile');
    }

    private function removeImages(Annonce $annonce)
    {
        $imgLi = $this->getDoctrine()->getRepository(Img::class)
            ->findBy(['ID_ANNONCE' => $annonce->getIdAnnonce()]);
        foreach ($imgLi as $image) {
            $filename = $image->getPath();
            $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
            if (file_exists($destination . "/" . $filename)) {
                unlink($destination . "/" . $filename);
            }
        }
    }

    /**
     * @Route("/modify", name="_modify")
     * @return Response
     */
    public function modify(Request $request)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)
            ->find($request->get('idAnnonce'));
        $form = $this->createForm(CreateAnnonceType::class, $annonce, ['tab' => $this->tabRubrique()]);
        $form->handleRequest($request);
        $imgLi = $annonce->getImgs();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get('image')) {
                $images = $request->files->get('image');// tableau d'images
                for ($i = 0; $i < count($images); $i++) {
                    //enregistre les fichiers images dans public/uploads
                    $uploadedFile = $images[$i];
                    $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
                    $uploadedFile->move($destination, $uploadedFile->getClientOriginalName());
                    //enregistre les fichiers dans la bdd
                    $img = new Img();
                    $img->setPath($uploadedFile->getClientOriginalName());
                    $annonce->addImg($img);
                }
            }
            $manager = $this->getDoctrine()->getManager();
            $manager->flush();
            return $this->redirectToRoute('profile');
        }
        return $this->render('profile/modifyAnnonce.html.twig', 
        [
            'form' => $form->createView(),
            'annonce' => $annonce,
            'imgLi' => $imgLi,
            'menu' => 'main/mainNav.html.twig'
        ]);
    }

    /**
     * @Route("/deleteImage", name="_delete_image")
     */
    public function deleteImage(Request $request)
    {
        $image = $this->getDoctrine()->getRepository(Img::class)
            ->findOneBy(['idImg' => $request->get('img')]);
        $manager = $this->getDoctrine()->getManager();
        $this->removeImage($image);
        $manager->remove($image);
        $manager->flush();
        return $this->redirectToRoute('profile');
    }

    //Supprime les images du disque d'une image
    private function removeImage(Img $image)
    {
        $filename = $image->getPath();
        $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
        if (file_exists($destination . "/" . $filename)) {
            unlink($destination . "/" . $filename);
        }
    }


    private function tabRubrique($idParent = null, $sub_mark = "", $tabrub = [])
    {
        $data = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findBy(['idParent' => $idParent], ['libelleRubrique' => 'ASC']);
        foreach ($data as $item) {
            $tabrub[$sub_mark . $item->getLibelleRubrique()] = $item;
            $tabrub = self::tabRubrique($item->getIdRubrique(), $sub_mark . '--', $tabrub);
        }
        return $tabrub;
    }

}
