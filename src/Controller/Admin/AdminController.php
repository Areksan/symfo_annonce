<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin", name="admin")
 */
class AdminController extends AbstractController
{
    /**
     * @Route("",name="")
     * @return \Symfony\Component\HttpFoundation\Response
     */

    public function adminIndex()
    {
        return $this->render('admin/index.html.twig', [
        ]);
    }
}
