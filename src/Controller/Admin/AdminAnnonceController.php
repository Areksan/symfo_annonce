<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use App\Entity\Img;
use App\Entity\Rubrique;
use App\Form\CreateAnnonceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/annonce", name="admin_annonce")
 */
class AdminAnnonceController extends AbstractController
{
    /**
     * @Route("", name="")
     */
    public function index()
    {
        $imgLi = [];
        $annoncesLi = $this->getDoctrine()->getRepository(Annonce::class)
            ->findAll();          
        foreach($annoncesLi as $ann){
            $imgLi[] = $this->getDoctrine()->getRepository(Img::class)->findOneBy(['ID_ANNONCE' => $ann->getIdAnnonce()]);
        }
        return $this->render('admin/admin_annonce/index.html.twig', [
            'imgLi' => $imgLi,
            'annonceLi' => $annoncesLi,
        ]);
    }

    /**
     * @Route("/delete", name="_delete")
     */
    public function adminDeleteAnnonce(Request $request)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)
            ->find($request->get('idAnnonce'));
        $manager = $this->getDoctrine()->getManager();
        $this->removeImages($annonce);
        $manager->remove($annonce);
        $manager->flush();
        return $this->redirectToRoute('admin_annonce');
    }

    //Supprime les images du disque d'une rubrique
    private function removeImages(Annonce $annonce)
    {
        $imgLi = $this->getDoctrine()->getRepository(Img::class)
            ->findBy(['ID_ANNONCE' => $annonce->getIdAnnonce()]);
        foreach ($imgLi as $image) {
            $filename = $image->getPath();
            $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
            if (file_exists($destination . "/" . $filename)) {
                unlink($destination . "/" . $filename);
            }
        }
    }

    /**
     * @Route("/modify",name="_modify")
     */
    public function adminModifyAnnonce(Request $request)
    {
        $annonce = $this->getDoctrine()->getRepository(Annonce::class)
            ->find($request->get('idAnnonce'));
        $form = $this->createForm(CreateAnnonceType::class, $annonce, ['tab' => $this->tabRubrique()]);
        $form->handleRequest($request);
        $imgLi = $annonce->getImgs();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($request->files->get('image')) {
                $images = $request->files->get('image');// tableau d'images
                for ($i = 0; $i < count($images); $i++) {
                    //enregistre les fichiers images dans public/uploads
                    $uploadedFile = $images[$i];
                    $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
                    $uploadedFile->move($destination, $uploadedFile->getClientOriginalName());
                    //enregistre les fichiers dans la bdd
                    $img = new Img();
                    $img->setPath($uploadedFile->getClientOriginalName());
                    $annonce->addImg($img);
                }
            }
            $manager = $this->getDoctrine()->getManager();
            $manager->flush();
            return $this->redirectToRoute('admin_annonce');
        }
        return $this->render('profile/modifyAnnonce.html.twig',
            [
                'form' => $form->createView(),
                'annonce' => $annonce,
                'imgLi' => $imgLi,
                'menu' => 'admin/navAdmin.html.twig',
            ]);
    }

    /**
     * @Route("/outdated",name="_outdated")
     */
    public function deleteOutdated()
    {
        $annonces = $this->getDoctrine()->getRepository(Annonce::class)->findAll();
        $now = new \DateTime();
        $now = $now->format('Y-m-d');
        $manager = $this->getDoctrine()->getManager();
        
        foreach ($annonces as $annonce) {
            $dateLim = $annonce->getDateLimAnnonce()->format('Y-m-d');
            if ($dateLim < $now && $dateLim != $now) {
                $manager->remove($annonce);
            }
        }
        $manager->flush();
        return $this->redirectToRoute('admin_annonce');
    }

    /**
     * @Route("/deleteImage", name="_delete_image")
     */
    public function deleteImage(Request $request)
    {
        $image = $this->getDoctrine()->getRepository(Img::class)
            ->findOneBy(['idImg' => $request->get('img')]);
        $manager = $this->getDoctrine()->getManager();
        $this->removeImage($image);
        $manager->remove($image);
        $manager->flush();
        return $this->redirectToRoute('profile');
    }
    private function removeImage(Img $image){
        $filename=$image->getPath();
        $destination = $this->getParameter('kernel.project_dir').'\public\uploads';
        if (file_exists($destination."/".$filename) ) {
            unlink($destination."/".$filename);
        }
    }

    private function tabRubrique()
    {
        $rubrique = $this->getDoctrine()
            ->getRepository(Rubrique::class)
            ->findAll();
        $tabrub = [];
        foreach ($rubrique as $item) {
            $tabrub[$item->getLibelleRubrique()] = $item;
        }
        return $tabrub;
    }
}
