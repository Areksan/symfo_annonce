<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use App\Entity\User;
use App\Form\UserModifyAdminType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/user", name="admin_user")
 */
class AdminUserController extends AbstractController
{
    /**
     * @Route("", name="")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $userLi = $this->getDoctrine()->getRepository(User::class)
            ->findAll();
        return $this->render('admin/admin_user/index.html.twig', [
            'userLi' => $userLi,
        ]);
    }

    /**
     * @Route("/delete",name="_delete")
     */
    public function delete(Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('user'));
        $annonceLi = $this->getDoctrine()->getRepository(Annonce::class)->findBy(['User' => $user]);
        $manager = $this->getDoctrine()->getManager();
        foreach ($annonceLi as $item) {
            $manager->remove($item);
        }
        $manager->remove($user);
        $manager->flush();
        return $this->redirectToRoute('admin_user');
    }

    /**
     * @Route("/modify",name="_modify")
     */
    public function modify(Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->find($request->get('user'));
        $roles = $this->getParameter("security.role_hierarchy.roles");
        if ($request->get('role')){
            $manager = $this->getDoctrine()->getManager();
            $user->setRoles($roles[$request->get('role')]);
            $manager->flush();
        }
        return $this->render('admin/admin_user/ModifyUser.html.twig',
            [
                'user' => $user,
                'role' => $roles,
            ]);
    }
}
