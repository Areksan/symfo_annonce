<?php

namespace App\Controller\Admin;

use App\Entity\Annonce;
use App\Entity\Rubrique;
use App\Entity\Img;
use App\Form\CreateRubriqueType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/rubrique", name="admin_rubrique")
 */
class AdminRubriqueController extends AbstractController
{
    /**
     * @Route("", name="")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function adminRubrique(Request $request)
    {
        $rubrique = new Rubrique();
        $form = $this->createForm(CreateRubriqueType::class, $rubrique, ['tab' => $this->tabRubrique()]);
        $form->handleRequest($request);
        $rubriqueLi = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findAll();
        if ($form->isSubmitted() && $form->isValid()) {
            $rubrique = $form->getdata();
            if (empty($rubriqueLi)) {//pas de rubrique enregistré
                $rubrique->setIdParent(null);
            } else {
                
                //ajout d'une rubrique de meme niveau que la selection
                if ($request->get("add")==="") {
                    $rubrique->setIdParent(null);
                }
            }
            $addForm = $this->getDoctrine()->getManager();
            $addForm->persist($rubrique);
            $addForm->flush();
            return $this->redirectToRoute('admin_rubrique');
        }
        return $this->render('admin/admin_rubrique/Rubrique.html.twig', [
            'form' => $form->createView(),
            'RubriqueLi' => $rubriqueLi,
            'CountAnn' => $this->countAnnonceInRub($rubriqueLi),
        ]);
    }

    /**
     * @Route("/Delete",name="_delete")
     */
    public function adminRemoveRubrique()
    {
        $request = Request::createFromGlobals();
        $rub = $this->getDoctrine()->getRepository(Rubrique::class)
            ->find($request->get('idrub'));
        $this->removeImages($rub);
        $this->removeChilds($rub);
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($rub);
        $manager->flush();
        return $this->redirectToRoute('admin_rubrique');
    }

    private function removeChilds(Rubrique $r)
    {
        $manager = $this->getDoctrine()->getManager();
        $data = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findBy(['idParent' => $r->getIdRubrique()]);
        foreach ($data as $rubrique) {
            $this->removeImages($rubrique);
            $manager->remove($rubrique);
            self::removeChilds($rubrique);
        }
    }

    //Supprime les images du disque d'une rubrique
    private function removeImages(Rubrique $rub)
    {
        $annonceLi = $this->getDoctrine()->getRepository(Annonce::class)
            ->findBy(['idRubrique' => $rub->getIdRubrique()]);
        if ($annonceLi) {
            foreach ($annonceLi as $annonce) {
                $imgLi = $this->getDoctrine()->getRepository(Img::class)
                    ->findBy(['ID_ANNONCE' => $annonce->getIdAnnonce()]);
                foreach ($imgLi as $image) {
                    $filename = $image->getPath();
                    $destination = $this->getParameter('kernel.project_dir') . '\public\uploads';
                    if (file_exists($destination . "/" . $filename)) {
                        unlink($destination . "/" . $filename);
                    }
                }
            }
        }
    }

    /**
     * @Route("/Modify",name="_modify")
     */
    public function adminModifyRubrique(Request $request)
    {
        $rub = $this->getDoctrine()->getRepository(Rubrique::class)
            ->find($request->get('idrub'));
        $form = $this->createForm(CreateRubriqueType::class, $rub);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $managerRub = $this->getDoctrine()->getManager();
            $managerRub->flush();
            return $this->redirectToRoute('admin_rubrique');
        }
        return $this->render('admin/admin_rubrique/ModifyRubrique.html.twig', [
            'form' => $form->createView(),
            'rub' => $rub,
        ]);
    }

    private function countAnnonceInRub($rubLi)
    {
        $tab = [];
        foreach ($rubLi as $item) {
            $tab[$item->getIdRubrique()] = $this->getDoctrine()->getRepository(Annonce::class)
                ->count(['idRubrique' => $item]);
        }
        return $tab;
    }

    private function tabRubrique($idParent = null, $sub_mark = "", $tabrub = [])
    {
        $data = $this->getDoctrine()->getRepository(Rubrique::class)
            ->findBy(['idParent' => $idParent], ['libelleRubrique' => 'ASC']);
        foreach ($data as $item) {
            $tabrub[$sub_mark . $item->getLibelleRubrique()] = $item;
            $tabrub = self::tabRubrique($item->getIdRubrique(), $sub_mark . '--', $tabrub);
        }
        return $tabrub;
    }
}
