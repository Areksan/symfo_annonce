<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DateAdd extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     */
    public $message = 'Invalide : La Date inscrite {{ value }} est inférieur à Aujourd\'hui {{ correct }}';
}
