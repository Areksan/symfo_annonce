<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateAddValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\DateAdd */

        if (null === $value || '' === $value) {
            return;
        }

        $t = new \DateTime();
        $t = new \DateTime($t->format('d-m-Y'));
        if ($value < $t) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ value }}', $value->format('d-m-Y'))
                ->setParameter('{{ correct }}', $t->format('d-m-Y'))
                ->addViolation();
        }
    }
}
