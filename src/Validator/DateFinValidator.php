<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DateFinValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint \App\Validator\DateFin */

        if (null === $value || '' === $value) {
            return;
        }

        // TODO: implement the validation here
        $now = new \DateTime();
        $now = new \DateTime($now->format('d-m-Y'));
        if ($value < $now ) {
            $this->context->buildViolation($constraint->message)
            ->setParameter('{{ value }}', $value->format('d-m-Y'))
                ->addViolation();
        }
    }
}
