<?php

namespace App\Form;

use App\Entity\Annonce;
use App\Validator\DateAdd;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateAnnonceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enTeteAnnonce', null, ['label' => 'Titre de votre Annonce'])
            ->add('corpsAnnonce', null, ['label' => 'Description de l\'annonce'])
            ->add('dateOnlineAnnonce', null,
                [
                    'label' => 'Date de dépot souhaitée',
                    'data' => new \DateTime(),
                    'constraints' => new DateAdd()
                ])
            ->add('dateLimAnnonce', null,
                [
                    'label' => 'Date de validité de l\'annonce',
                    'data' => new \DateTime(),
                    'constraints' => new DateAdd(),
                ])
            ->add('IdRubrique', ChoiceType::class, [
                'choices' => $options['tab'],
                'label' => 'Rubrique'
            ]);
//            ->add('idImg', FileType::class, ['label' => 'Img'])
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Annonce::class,
            'tab' => null,
            'enctype' => "multipart/form-data",
        ]);
        $resolver->setAllowedTypes('tab', 'array');
    }
}
